﻿using MongoDB.Bson;
using MongoDB.Driver;
using PizzaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization;
using MongoDB.Driver.Builders;

namespace PizzaApp.Repository
{
    public class IRepository<T> where T : class
    {
        private MongoDatabase _database;
        private string _tableName;
        private MongoCollection<T> _collection;

       

        public IRepository(MongoDatabase db, string table)
        {
            _database = db;
            _tableName = table;
            _collection = _database.GetCollection<T>("pizza");
        }

        public T Get(string i)
        {
            IMongoQuery query = Query.EQ("_id", ObjectId.Parse(i));
            return _collection.Find(query).FirstOrDefault();
    
        }

        public IQueryable<T> GetAll()
        {
            MongoCursor<T> cursor = _collection.FindAll();
            return cursor.AsQueryable<T>();

        }

    }
}