﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using PizzaApp.Services;
using Newtonsoft.Json.Linq;
using System.Web.Http.Cors;

namespace PizzaApp.API.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PizzaController : ApiController
    {

        private readonly IPizzaService _pizzaService;
        private readonly IJObjectService _jobjectservice;
        private readonly IChainOfResponsibilty _chainofresponsibilty;



        public PizzaController()
        {
            _pizzaService = new PizzaService();
            _jobjectservice = new JObjectService();
            _chainofresponsibilty = new ChainOfResponsibility();


        }

       
        public HttpResponseMessage Get(string id)
        {
          
            var pizza = _pizzaService.Get(id);
            if (pizza != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, pizza);
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Nie ma takiej pizzy.");
        }

        public HttpResponseMessage GetAll()
        {
            var pizzas = _pizzaService.GetAll();
            if (pizzas != null)
                return Request.CreateResponse(HttpStatusCode.OK, pizzas);
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Żadna pizza nie została odnaleziona.");
        }

        [Route("api/Pizza/GetTranslation/{id}/{lang}")]
        public HttpResponseMessage GetTranslation(string id,string lang)
        {
           
            var pizza = _pizzaService.Get(id);
            if (pizza != null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, _chainofresponsibilty.ChainOfResponsibilty(lang,pizza));
            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Nie ma takiej pizzy");
        }

        [Route("api/Pizza/GetAllTranslation/{lang}")]
        public HttpResponseMessage GetAllTranslation(string lang)
        {
            JArray jArray = new JArray();

            var pizzas = _pizzaService.GetAll();
            if (pizzas != null)
            {
                foreach (var pizza in pizzas)
                {                 
                        jArray.Add(_jobjectservice.JObjectConvert(_chainofresponsibilty.ChainOfResponsibilty(lang, pizza)));                 
                }

                return Request.CreateResponse(HttpStatusCode.OK, jArray);

            }
            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Nie ma nic.");
        }



    }
}
