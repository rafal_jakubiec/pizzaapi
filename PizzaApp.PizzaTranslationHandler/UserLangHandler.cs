﻿using PizzaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace PizzaApp.Handler
{
    public class UserLangHandler : HandlerPizza
    {
        public override PizzaContentDto HandleRequest(string lang, Pizza pizza )
        {
           
                return pizza.Contents.Where(t => t.LanguageCode == lang).Any() ? pizza.Contents.Where(t => t.LanguageCode ==lang ).Select(p => new PizzaContentDto()
                {
                    Description = p.Description,
                    LanguageCode = p.LanguageCode,
                    Name = p.Name
                }).FirstOrDefault() : successor.HandleRequest(lang, pizza);
            }
          
        
    }
}