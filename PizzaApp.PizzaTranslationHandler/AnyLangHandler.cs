﻿using PizzaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PizzaApp.Handler
{
    public class AnyLangHandler: HandlerPizza
    {
        public override PizzaContentDto HandleRequest(string lang, Pizza pizza)
        {

            return pizza.Contents.Select(p => new PizzaContentDto()
            {
                Description = p.Description,
                LanguageCode = p.LanguageCode,
                Name = p.Name
            }).FirstOrDefault();
        }
    }
}