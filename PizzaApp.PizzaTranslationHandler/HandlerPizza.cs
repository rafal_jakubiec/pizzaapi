﻿using PizzaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PizzaApp.Handler
{
    public abstract class HandlerPizza
    {
        protected HandlerPizza successor;

        public void SetSuccessor(HandlerPizza successor)
        {
            this.successor = successor;
        }
        
        public abstract PizzaContentDto HandleRequest(string lang, Pizza pizza);
    }
}