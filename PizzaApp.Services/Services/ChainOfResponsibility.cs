﻿using PizzaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PizzaApp.Handler;

namespace PizzaApp.Services
{
    public class ChainOfResponsibility : IChainOfResponsibilty
    {
        public PizzaContentDto ChainOfResponsibilty(string lang,Pizza pizza)
        {
            HandlerPizza h1 = new UserLangHandler();
            HandlerPizza h2 = new EnLangHandler();
            HandlerPizza h3 = new PlLangHandler();
            HandlerPizza h4 = new AnyLangHandler();
            h1.SetSuccessor(h2);
            h2.SetSuccessor(h3);
            h3.SetSuccessor(h4);

            return h1.HandleRequest(lang, pizza);
        }
    }
}