﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PizzaApp.UnitOfWork;
using PizzaApp.Model;
using MongoDB.Bson;
using System.Threading.Tasks;

namespace PizzaApp.Services
{
    public class PizzaService : IPizzaService
    {
        private readonly PizzaUnitOfWork _pUnitOfwork;

        public PizzaService()
        {
            _pUnitOfwork = new PizzaUnitOfWork();
        }

        public Pizza Get(string i)
        {
            return _pUnitOfwork.Pizza.Get(i);
        }
        public IQueryable<Pizza> GetAll()
        {
            return _pUnitOfwork.Pizza.GetAll();
        }
    }
}