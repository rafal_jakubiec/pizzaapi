﻿using Newtonsoft.Json.Linq;
using PizzaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaApp.Services
{
    public interface IJObjectService
    {
       JObject JObjectConvert(PizzaContentDto data);
    }
}
