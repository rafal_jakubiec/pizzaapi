﻿using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PizzaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PizzaApp.Services
{
    public class JObjectService : IJObjectService
    {
        public JObject JObjectConvert(PizzaContentDto data)
        {
            var _data = data.ToJson();     
            return (JObject)JsonConvert.DeserializeObject(_data.ToString());
        }
    }
}