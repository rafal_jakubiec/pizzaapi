﻿using PizzaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaApp.Services
{
    public interface IChainOfResponsibilty
    {
        PizzaContentDto ChainOfResponsibilty(string lang,Pizza pizza);
    }
}
