﻿using System.Linq;
using PizzaApp.Model;

namespace PizzaApp.Services
{
    public interface IPizzaService
    {
        Pizza Get(string i);
        IQueryable<Pizza> GetAll();
    }

}