﻿using MongoDB.Driver;
using PizzaApp.Model;
using PizzaApp.Repository;
using System.Configuration;

namespace PizzaApp.UnitOfWork
{
    public class PizzaUnitOfWork
    {

        private MongoDatabase _database;

        protected IRepository<Pizza> _pizza;

        public PizzaUnitOfWork()
        {
            var connectionString = ConfigurationManager.AppSettings["MongoDBConectionString"];
            var client = new MongoClient(connectionString);
           var server = client.GetServer();
            var databaseName = ConfigurationManager.AppSettings["MongoDBDatabaseName"];
            _database = server.GetDatabase(databaseName);
        }
        public IRepository<Pizza> Pizza
        {
            get
            {
                if (_pizza == null)
                    _pizza = new IRepository<Pizza>(_database, "Pizza");

                return _pizza;
            }
        }

    }
}